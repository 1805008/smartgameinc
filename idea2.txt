Observe the World to Get Game Ideas
The world is an incredible, amazing place. And there are a lot of really bizarre people in it. And sometimes these people do really weird things, and it turns out great!

Try watching what’s going on around you.

Start spending time each day really paying attention to people’s interactions. And not just the interactions they have with each other, but the interactions they have with the world. Does anything stand out? Would any of it make a good game?