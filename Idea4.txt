Monkey See, Monkey Do
Maybe you saw this one coming: Think about video games! What’s your favorite game?

What are the games you’ve played that you loved while you played them?

Existing games can give awesome ideas for video games you are planning to make. Of course, you cannot copy them outright. But using them for inspiration is a perfectly viable option!

Get a Patent
By definition, a patent is a protection for your intellectual property (that you invented) from being copied. In the case of your game idea, in the form of a patent, it is considered your invention.

A patent is unique in the fact that it can protect your idea.

The concept for your game that is uniquely yours can be patented and the idea behind it protected.

That way, if during the course of your game development, one of your friends or coworkers helping you out tries to market their own version using your idea, they would be in violation of your patent and you could take them to court to make them stop.

Copyrights
Game design copyrights differ from a patent in the fact that they protect the actual expression of your idea. For example, if Tic-Tac-Toe is patented using X’s and O’s, you could still create a game using apples and oranges because the expression of the idea is different.

You can register your copyright officially by registering with the United States Copyright Office, which can offer legal protection as well.

new idea added

